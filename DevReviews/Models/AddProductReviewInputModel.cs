namespace DevReviews.Models
{
    public class AddProductReviewInputModel
    {
        public int Rating { get; set; }
        public string Author { get; set; }
        public string Comment { get; set; }
    }
}