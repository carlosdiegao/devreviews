using DevReviews.Models;
using Microsoft.AspNetCore.Mvc;

namespace DevReviews.Controllers
{
    [ApiController]
    [Route("api/products/{productId}/productReviews")]
    public class ProductReviewController : ControllerBase
    {
        //LER UM ITEM ATRAVÉS DE UM ID
        //GET api/products/1/productreviews/5
        [HttpGet("{id}")]
        public IActionResult GetById(int productId, int id)
        {
            //Se não existir com o id especificado, retornar notfound
            return Ok();
        }
        
        //CADASTRAR NOVO REVIEW
        //POST api/products/1/productreviews
        [HttpPost]
        public IActionResult Post(int productId, AddProductReviewInputModel model)
        {
            //Retorna 201: codigo de que foi cadastrado.
            return CreatedAtAction(nameof(GetById), new {id=1, productId = 2}, model);
            
        }
        
        


    }
}