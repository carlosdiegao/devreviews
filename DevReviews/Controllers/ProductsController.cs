using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DevReviews.Entities;
using DevReviews.Models;
using DevReviews.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace DevReviews.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly DevReviewsDbContext _dbContext;
        private readonly IMapper _mapper;

        public ProductsController(DevReviewsDbContext dbContext, IMapper mapper)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var products = _dbContext.Products;
            //var productsViewModel = products.Select(p => new ProductViewModel(p.Id, p.Title, p.Price));
            var productsViewModel = _mapper.Map<List<ProductDetailsViewModel>>(products);
            return Ok(productsViewModel);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var product = _dbContext.Products.SingleOrDefault(p => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }

/*           var reviews = product
                .Reviews
                .Select(r => new ProductReviewViewModel(r.Id, r.Author, r.Rating, r.Comments, r.RegisterAt))
                .ToList();

            var productDetails = new ProductDetailsViewModel(
                product.Id,
                product.Title,
                product.Description,
                product.Price,
                product.RegisteredAt,
                reviews
            );
*/

            var productDetails = _mapper.Map<ProductDetailsViewModel>(product);
            return Ok(productDetails);
        }

        //Cadastro de Produto
        [HttpPost]
        public IActionResult Post(AddProductInputModel model)
        {
            var product = new Product(model.Title, model.Description, model.Price);
            _dbContext.Products.Add(product);
            return CreatedAtAction(nameof(GetById), new {id = product.Id}, model);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, UpdateProductInputModel model)
        {
            // Se houverem erros de validação, retornar BadRequest
            // Se não existir o produto com o id especificado, retornar NotFound()
            if (model.Description.Length > 50)
            {
                return BadRequest();
            }

            var product = _dbContext.Products.SingleOrDefault(p => p.Id == id);

            if (product == null)
            {
                return NotFound();
            }

            product.Update(model.Description, model.Price);

            return NoContent();
            
        }
    }
}