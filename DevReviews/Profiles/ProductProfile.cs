using AutoMapper;
using DevReviews.Entities;
using DevReviews.Models;

namespace DevReviews.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            CreateMap<ProductReview, ProductReviewViewModel>();
            CreateMap<Product, ProductViewModel>();
            CreateMap<Product, ProductDetailsViewModel>();

        }
    
        
    }
}