using System.Collections.Generic;
using DevReviews.Entities;

namespace DevReviews.Persistence
{
    public class DevReviewsDbContext
    {
        public DevReviewsDbContext()
        {
            Products = new List<Product>();
        }
        
        public List<Product> Products { get; set; }
        
        
        
    }
}